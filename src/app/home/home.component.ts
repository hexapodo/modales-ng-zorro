import { Component, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  modal!: NzModalRef;

  // variable para recibir el resultado de
  // la ventana modal
  resultadoModal: {result: null | string} = { result: null};

  constructor(
    private nzModalService: NzModalService,
    private viewContainerRef: ViewContainerRef,
  ) { }

  ngOnInit(): void {
  }

  abrirModal(modalFooter: TemplateRef<{}>) {
    this.modal = this.nzModalService.create<ModalComponent>({
      nzTitle: "este es el titulo de la ventana",
      nzContent: ModalComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        laVariable: 'hola',
      },
      nzFooter: modalFooter,
      nzClosable: false,
      nzMaskClosable: false,
      nzKeyboard: false,
    });

    this.modal.afterClose.subscribe({
      next: (rta) => {
          console.log(rta);
      },
  });
  }

  cancelar() {
    this.resultadoModal = {
      result: null
    };
    this.modal.destroy(this.resultadoModal);
    console.log('cancelar')
  }

  guardar() {
    console.log('guardar')
    this.resultadoModal = {
      result: this.modal.componentInstance.variableInterna
    };
    this.modal.close(this.resultadoModal);
  }

}
