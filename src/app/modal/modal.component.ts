import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  laVariable!: string;
  variableInterna: string = 'valor inicial';

  constructor(
    private modal: NzModalRef
  ) { }

  ngOnInit(): void {
  }


}
